package br.com.jogo21;

public enum Numero {

	A(1), DOIS(2), TRES(3), QUATRO(4), CINCO(5), SEIS(6), SETE(7), OITO(8), NOVE(9), DEZ(10), Valete(10), Dama(10),
	Reis(10);

	private int valorCarta;

	Numero(int valorCarta) {
		this.valorCarta = valorCarta;
	}

	public int getValorCarta() {
		return valorCarta;
	}

}
