package br.com.jogo21;

public class Carta {

	private Naipe naipe;
	private Numero numero;

	public Carta(Naipe naipe, Numero numero) {
		this.naipe = naipe;
		this.numero = numero;
	}
	
	public Naipe getNaipe() {
		return naipe;
	}

	public Numero getNumero() {
		return numero;
	}

	public String toString() {
		return numero+" - "+naipe.getValorNaipe() + " - pontuacao :"+numero.getValorCarta()+"\n";
	}

}
