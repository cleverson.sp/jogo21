package br.com.jogo21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baralho {

	List<Carta> cartas = new ArrayList<>();

	public Baralho() {

		for (Numero numero : Numero.values()) {
			for (Naipe naipe : Naipe.values()) {
				Carta carta = new Carta(naipe, numero);
				cartas.add(carta);
			}
		}
		Collections.shuffle(cartas);

	}

	public Carta retirarCarta() {
		int posicao = (int) (Math.random() * cartas.size());
		return cartas.remove(posicao);
	}

	public List<Carta> retirarCarta(int qtde) {

		List<Carta> cartas = new ArrayList<>();
		for (int i = 0; i < qtde; i++) {
			cartas.add(retirarCarta());
		}
		return cartas;
	}

	@Override
	public String toString() {
		StringBuilder build = new StringBuilder();

		for (Carta carta : cartas) {
			build.append(carta);
			build.append("\n");
			System.out.println(build);
		}

		return build.toString();

	}
}
