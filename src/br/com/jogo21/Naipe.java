package br.com.jogo21;

public enum Naipe {
	OURO("Ouro"), ESPADA("Espada"), COPAS("Copas"), PAUS("Paus");

	private String valorNaipe;

	Naipe(String valorNaipe) {
		this.valorNaipe = valorNaipe;
	}

	public String getValorNaipe() {
		return valorNaipe;
	}

}
