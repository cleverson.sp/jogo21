package br.com.jogo21;

import java.util.List;

public class main {
	public static void main(String[] args) {

		Console console = new Console();
		Jogada jogada = new Jogada();

		String continua = "s";

		String iniciaJogo = console.entradaInput();

		if (iniciaJogo.equals("s")) {
			List<Carta> start = jogada.JogadaUm(2);
			Console.imprimir(start, jogada);

			while (continua.equals("s")) {
				System.out.println("------->" + continua);
				continua = Console.continueInput();

				if (continua.equals("n")) {
					Console.imprimir("Jogo finalizado");
					Console.imprimir(continua, start, jogada);
					break;
				}

				Carta next = jogada.jogar();
				Console.imprimir(next, jogada);

			}

		} else {
			Console.imprimir("Jogo finalizado");

		}

	}
}
