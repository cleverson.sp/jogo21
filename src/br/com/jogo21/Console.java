package br.com.jogo21;

import java.util.List;
import java.util.Scanner;

public class Console {

	private static Scanner scanner = new Scanner(System.in);

	public static String entradaInput() {
		imprimir("Deseja iniciar o jogo (digite s para sim)?");

		String escolha = scanner.nextLine();

		return escolha;

	}

	public static String continueInput() {
		
		imprimir("Deseja Continuar a jogar? o jogo (digite s para sim)?");
		String escolha = scanner.nextLine();
		return escolha;
	}

	public static void imprimir(Carta cartas, Jogada ponto) {

		System.out.println("===== Console Jogo =====\n" + cartas + "  Pontuacao :" + ponto.getPontos()
				+ "\n===== Console Jogo =====");
	}

	public static void imprimir(List<Carta> cartas, Jogada ponto) {
		System.out.println("===== Console Jogo =====\n");
		for (Carta carta : cartas) {
			System.out.println("Carta :" + carta);
		}
		System.out.println("Pontuacao :" + ponto.getPontos() + "\n===== Console Jogo =====");
	}

	public static void imprimir(String finaliza, List<Carta> carta, Jogada ponto) {

		System.out.println("===== Fim de Jogo =====\n Voce finalizou com a Pontuacao :" + ponto.getPontos()
				+ "\n===== Fim de Jogo =====");
	}

	public static void imprimir(Object texto) {
		System.out.println(texto);
	}

}
