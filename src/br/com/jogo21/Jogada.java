package br.com.jogo21;

import java.util.Collection;
import java.util.List;
import br.com.jogo21.Carta;

public class Jogada {

	Baralho baralho = new Baralho();
	private int pontos = 0;
	private List<String> coletaCartas;

	public List<Carta> JogadaUm(int qtde) {
		List<Carta> cartas = baralho.retirarCarta(qtde);

		for (Carta carta : cartas) {
			pontos += carta.getNumero().getValorCarta();
		}

		return cartas;
	}

	public Carta jogar() {

		Carta cartas = baralho.retirarCarta();
		pontos += cartas.getNumero().getValorCarta();

		return cartas;

	}

	public int getPontos() {
		return pontos;
	}

	public boolean validaPontuacao() {
		if (pontos >= 21) {

			return false;
		}
		return true;
	}

	public List<String> getcoletaCartas() {
		return coletaCartas;
	}

}
